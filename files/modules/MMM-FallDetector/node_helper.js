var NodeHelper = require("node_helper");
const net = require('net');

module.exports = NodeHelper.create({
    start: function () {
        var self = this;

        function handler(data) {
            console.log("Receiving data")
            console.log(data[0]);   // The function returns the product of p1 and p2
            data = data[0];
            self.sendSocketNotification("FALL_INFO", { value: data });
        }

        function handleConnection(conn) {
            var remoteAddress = conn.remoteAddress + ':' + conn.remotePort;
            console.log('new client connection from %s', remoteAddress);
            conn.on('data', handler);
        }

        console.log("Hello")
        var server = net.createServer();
        server.on('connection', handleConnection)

        server.listen(8001, '127.0.0.1');
    },
})
