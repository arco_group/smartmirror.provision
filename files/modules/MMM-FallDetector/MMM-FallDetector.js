Module.register("MMM-FallDetector",{
	// Default module config.
	defaults: {
		header: "Fall Detector"
	},
	screen_message: "Everything is ok",
	fall: 0,

	// Override dom generator.
	getDom: function() {
		var wrapper = document.createElement("div");
		if(this.fall == 0) {
			this.screen_message = "Everything is ok"
		} else {
			this.screen_message = "A fall is detected"
		}
		wrapper.innerHTML = this.screen_message;
		return wrapper;
	},

	start: function() {
		var self = this;
		this.sendSocketNotification('SET_COMM')
		setInterval(function() {
			self.updateDom(); // no speed defined, so it updates instantly.
		}, 1000); //perform every 1000 milliseconds.
	},

	socketNotificationReceived: function(notification, payload) {
		this.fall = payload.value;	
	},

	getHeader: function() {
		return this.config.header;
	},
});